﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Enemy : MonoBehaviour
{

    public int health = 100;
    public Manager manager;

    // Start is called before the first frame update
    void Awake()
    {
        manager = GameObject.FindObjectOfType<Manager>();

        if (manager == null)
        {
            Debug.Log("Dont Find Manager");
        }
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    public void Damage(int amountOfDamage)
    {
        health -= amountOfDamage;

      //  Debug.Log($"Current Health: { health} ");

        if (health < 0)
        {
            health = 0;

            //   Debug.LogWarning($"Health is: {health}");
            manager.AddEnemyToCounter();
            Destroy(gameObject);
        }
    }
}
