﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;

public class TowerBase : MonoBehaviour, IPointerEnterHandler, IPointerExitHandler, IPointerClickHandler
{
    public GameObject towerGhost;
    public GameObject towerPrefab;

    private Manager manager;
    

    public void OnPointerClick(PointerEventData eventData)
    {
        Instantiate(towerPrefab, transform.position, transform.rotation);

        towerGhost.SetActive(false);

        GetComponent<MeshCollider>().enabled = false;

        Time.timeScale = 1f;

        manager.panelNewTurret.SetActive(false);

        manager.turretManager.UnregisterTurret(this);

        manager.turretManager.DeactivateALLBases();
    }

    public void OnPointerEnter(PointerEventData eventData)
    {
        towerGhost.SetActive(true);

        Debug.Log("is on");
    }

    public void OnPointerExit(PointerEventData eventData)
    {
        towerGhost.SetActive(false);

        Debug.Log("is off");
    }


    // Start is called before the first frame update
    void Start()
    {
        manager = GameObject.FindObjectOfType<Manager>();

        if (manager == null)
        {
            Debug.Log("Manager dose not exist");
        }

        manager.turretManager.RegisterTurret(this);
    }

    // Update is called once per frame
    void Update()
    {
        
    }
}
