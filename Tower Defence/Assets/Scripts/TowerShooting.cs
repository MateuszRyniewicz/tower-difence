﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TowerShooting : MonoBehaviour
{

    public GameObject bulletPrefab;

    public void OnEnable()
    {
        InvokeRepeating("Fire", 0f, 0.5f);
    }


    public void OnDisable()
    {
        CancelInvoke("Fire");
    }


    public void Fire()
    {
        Instantiate(bulletPrefab, transform.position, transform.rotation);
    }

}
