﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Manager : MonoBehaviour
{
    public GameObject panelStart;

    public GameObject panelGameOver;

    public GameObject panelWin;

    public GameObject panelNewTurret;


    public TurretManager turretManager;

    public Transform ourBase;
    


    int currentWave = 0;

    public int[] waves;

    [SerializeField]
    int counter;

    // Start is called before the first frame update
    void Start()
    {

        panelStart.SetActive(true);

        panelGameOver.SetActive(false);

        panelNewTurret.SetActive(false);

        panelWin.SetActive(false);

        turretManager.DeactivateALLBases();

        Time.timeScale = 0f;

    }

    public void Play()
    {
        panelStart.SetActive(false);

        panelNewTurret.SetActive(true);

        turretManager.ActivateAllBases();



       // Time.timeScale = 1f;
    }


    public void GameOver()
    {
        panelGameOver.SetActive(true);

        Time.timeScale = 0;
    }

    public void WaveFinished()
    {
        panelWin.SetActive(true);
        Time.timeScale = 0;
    }

    public void PlayAgain()
    {
        DestroyAllEnemies();

        Time.timeScale = 1f;

        panelGameOver.SetActive(false);
    }

    public void DestroyAllEnemies()
    {
        GameObject[] enemies = GameObject.FindGameObjectsWithTag("Enemy");

        if (enemies.Length == 0)
        {
            return;
        }

        for(int i = 0; i < enemies.Length; i++)
        {
            Destroy(enemies[i]);
        }
    }
    public void AddEnemyToCounter()
    {
        counter++;

        isWaveIsFinished();
    }


    public void PlayNextWave()
    {
        panelWin.SetActive(false);

        panelNewTurret.SetActive(true);

        turretManager.ActivateAllBases();

      //  Time.timeScale = 1;

        DestroyAllEnemies();

        currentWave++;

        counter = 0;
    }


    bool isWaveIsFinished()
    {
        if (counter >= waves[currentWave])
        {
            Debug.Log("wave is Finish");

            WaveFinished();

            return true;
        }
        return false;


        
    }
}



