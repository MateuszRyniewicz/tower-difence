﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Base : MonoBehaviour
{
    public Manager manager;
    // Start is called before the first frame update
    void Start()
    {
        manager.ourBase = transform;   
    }

    private void OnTriggerEnter(Collider other)
    {
        if (other.tag == "Enemy")
        {
            
            Time.timeScale = 0f;

            manager.GameOver();

        }
    }
}
